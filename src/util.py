import os
import shutil
import logging
import requests
from zipfile import ZipFile
import torch.nn as nn
import torch.optim as optim
from matplotlib import pyplot as plt

logging.basicConfig(format='%(asctime)s - %(levelname)s: %(message)s', datefmt='%d/%m/%Y %I:%M:%S %p',
                    level=logging.INFO)

class InitTypes:
    Normal              = "normal"
    Xavier              = "xavier"
    Kaiming             = "kaiming"
    Orthogonal          = "orthogonal"

class LRSchedulerTypes:
    Linear              = "linear"
    Step                = "step"
    Plateau             = "plateau"
    Cosine              = "cosine"

def clean_make_dir(path):
    if os.path.isdir(path):
        logging.info("Clean selected - removing: {}".format(path))
        shutil.rmtree(path)
    logging.info("Creating output directory: {}".format(path))
    return os.makedirs(path, exist_ok=True)

def download_dataset(name="horse2zebra", output="./datasets"):
    dataset_base_path = "https://people.eecs.berkeley.edu/~taesung_park/CycleGAN/datasets"
    dataset_url_path = "{0}/{1}.zip".format(dataset_base_path, name)
    to_path = "{0}/{1}".format(output, name)
    clean_make_dir(to_path)
    logging.info("Downloading {0} dataset from {1}".format(name, dataset_url_path))
    response = requests.get(dataset_url_path)
    if response.status_code == 200:
        with open("{0}/{1}.zip".format(to_path, name), "wb") as f:
            f.write(response.content)
        logging.info("Downloaded to {0}".format(to_path))
        logging.info("Extracting to {0}".format(to_path))
        with ZipFile("{0}/{1}.zip".format(to_path, name), "r") as zf:
            zf.extractall(output)
        logging.info("Done extracting")
    else:
        response.raise_for_status()
    return

def init_layer_weights(net, init_type=InitTypes.Normal, init_gain=0.02):
    """
    init_type - options are normal, xavier, kaiming, orthogonal
    """
    def _init_weights(n):
        clsname = n.__class__.__name__
        if hasattr(n, "weight") and (clsname.find("Conv") != -1 or clsname.find("Linear") != -1):
            if init_type == InitTypes.Normal:
                nn.init.normal_(n.weight.data, 0.0, init_gain)
            elif init_type == InitTypes.Xavier:
                nn.init.xavier_normal_(n.weight.data, gain=init_gain)
            elif init_type == InitTypes.Kaiming:
                nn.init.kaiming_normal_(n.weight.data, a=0, mode="fan_in")
            elif init_type == InitTypes.Orthogonal:
                nn.init.orthogonal_(n.weight.data, gain=init_gain)
            else:
                raise NotImplementedError("Init method %s not yet implemented" % init_type)

            if hasattr(n, "bias") and n.bias is not None:
                nn.init.constant_(n.bias.data, 0.0)
        elif clsname.find("BatchNorm2d") != -1:
            nn.init.normal_(n.weight.data, 1.0, init_gain)
            nn.init.constant_(n.bias.data, 0.0)
    net.apply(_init_weights)
    return

def get_lr_scheduler(optimizer, policy=LRSchedulerTypes.Linear, num_iterations=None, num_iterations_decay=None, lr_decay_iterations=None):
    """
    policy - options are linear, step, plateau, cosine
    num_iterations - number of iterations to keep the learning rate constant
    num_iterations_decay - number of iterations to decay the learning rate to zero
    lr_decay_iterations - multiply by a gamma every # of iterations
    """
    if policy == LRSchedulerTypes.Linear:
        sched = optim.lr_scheduler.LambdaLR(optimizer, lr_lambda=lambda epoch: (1.0 - max(0, epoch - num_iterations) / float(num_iterations_decay + 1)))
        return sched
    elif policy == LRSchedulerTypes.Step:
        return optim.lr_scheduler.StepLR(optimizer, step_size=lr_decay_iterations, gamma=0.1)
    elif policy == LRSchedulerTypes.Plateau:
        return optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode="min", factor=0.2, threshold=0.01, patience=5)
    elif policy == LRSchedulerTypes.Cosine:
        return optim.lr_scheduler.CosineAnnealingLR(optimizer, T_max=num_iterations, eta_min=0)
    else:
        raise NotImplementedError("Learning rate policy %s not yet implemented" % policy)
    return None

def get_linear_lr_scheduler(optimizer, num_iterations, num_iterations_decay):
    sched = optim.lr_scheduler.LambdaLR(optimizer, lr_lambda=lambda epoch: (1.0 - max(0, epoch - num_iterations) / float(num_iterations_decay + 1)))
    return sched

def plot_losses(losses_dict: 'dict', name=None):
    plt.figure(figsize=(10, 5))
    plt.title("Losses over time")
    for lname, lvals in losses_dict.items():
        plt.plot(lvals, label=lname)
    plt.xlabel("Iterations")
    plt.ylabel("Loss")
    plt.legend()
    if name:
        plt.savefig(name)
    plt.show()
    return

def plot_scores(scores, name=None):
    plt.figure(figsize=(10, 5))
    plt.title("Scores over time")
    plt.plot(scores)
    plt.xlabel("Iterations")
    plt.ylabel("Scores")
    plt.legend()
    if name:
        plt.savefig(name)
    plt.show()
    return
