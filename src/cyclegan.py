import os
import torch
from torch import nn
from torch import optim
from torchvision import utils as tutils
from itertools import chain
from models import ResnetGenerator, NLayerDiscriminator, GANLoss, ImagePool
from datasets import UnalignedDataset, GANDirection
from util import init_layer_weights, InitTypes, get_linear_lr_scheduler

class CycleGAN():
    """
    Basic principle - two GANs, A and B.
    A takes input from collection 1, and generates photos similar to collection 2. Discriminator measures how similar they are to collection 2.
    B takes input from collection 2, and generates photos similar to collection 1. Discriminator measures how similar they are to collection 1.

    A's output is fed to B, and the reconstruction loss is measured too - and vice versa.

    Cycle consistency loss measures the L1 distance of an input image from the generated image.
    In this case we measure the difference between the real image, and the reconstructed image.

    Using the horse2zebra dataset as an example, with GAN direction as A to B.
    Gen A produces images of zebras, given images of horses, Gen B produces images of horses given zebras.
    Their respective discriminators attempt to distinguish each image from a real example.
    The output of Gen A is fed to Gen B to create reconstruction of the original input to A - and the exact same process is repeated from B to A,
    with Gen A attempting to reconstruct Gen B's original input.

    Original paper used following settings (https://arxiv.org/pdf/1703.10593.pdf):
        Batch size           = 1
        Learning Rate        = 0.0002
        Num Iterations       = 100
        Num Iterations Decay = 100
        LR Scheduler         = Linear
        Image Pool size      = 50
        Num Resnet Blocks    = 9 (for 256x256 images)
        Optimizer            = Adam
        Lambda               = 10
        GAN Loss             = Least Squares loss
    """
    def __init__(self, train, num_iterations=10, num_iterations_decay=0, lr_decay_iterations=0, input_num_channels=3, output_num_channels=3, num_filters=32, num_D_layers=3, lambda_identity=0.5, direction=GANDirection.A2B, device=torch.device("cpu")):
        self.train = train
        self.device = device
        self.direction = direction
        self.lambda_identity = lambda_identity
        self.loss_names = ['D_A', 'D_B', 'cycle_A', 'cycle_B', 'G_A', 'G_B', 'idt_A', 'idt_B']
        self.visual_names = ['real_A', 'real_B', 'fake_A', 'fake_B', 'rec_A', 'rec_B']
        if train:
            if self.lambda_identity > 0.0:
                self.visual_names.extend(['idt_A', 'idt_B'])
            self.model_names = ['G_A', 'G_B', 'D_A', 'D_B']
        else:
            self.model_names = ['G_A', 'G_B']

        self.net_G_A = ResnetGenerator(input_num_channels=input_num_channels, output_num_channels=output_num_channels, num_filters=num_filters, num_blocks=9, padding_type="reflect", reduce_checkerboard=True)
        self.net_G_A.to(device=device)
        init_layer_weights(self.net_G_A, InitTypes.Normal)

        self.net_G_B = ResnetGenerator(input_num_channels=input_num_channels, output_num_channels=output_num_channels, num_filters=num_filters, num_blocks=9, padding_type="reflect", reduce_checkerboard=True)
        self.net_G_B.to(device=device)
        init_layer_weights(self.net_G_B, InitTypes.Normal)

        if train:
            self.net_D_A = NLayerDiscriminator(input_num_channels=input_num_channels, num_filters=num_filters, num_layers=num_D_layers)
            self.net_D_A.to(device=device)
            init_layer_weights(self.net_D_A, InitTypes.Normal)

            self.net_D_B = NLayerDiscriminator(input_num_channels=input_num_channels, num_filters=num_filters, num_layers=num_D_layers)
            self.net_D_B.to(device=device)
            init_layer_weights(self.net_D_B, InitTypes.Normal)

        if train:
            if self.lambda_identity > 0.0:
                assert input_num_channels == output_num_channels

            self.fake_A_pool = ImagePool(50)
            self.fake_B_pool = ImagePool(50)

            self.criterion = GANLoss("lsgan", target_real_label=1, target_fake_label=0)
            self.criterion.to(device=device)

            self.criterion_cycle = nn.L1Loss()
            self.criterion_idt = nn.L1Loss()

            self.optimizer_G = optim.Adam(chain(self.net_G_A.parameters(), self.net_G_B.parameters()), lr=0.0002, betas=(0.5, 0.999))
            self.optimizer_D = optim.Adam(chain(self.net_D_A.parameters(), self.net_D_B.parameters()), lr=0.0002, betas=(0.5, 0.999))

            self.schedulers = [get_linear_lr_scheduler(o, num_iterations, num_iterations_decay) for o in [self.optimizer_G, self.optimizer_D]]
        else:
            self.optimizer_G = None
            self.optimizer_D = None
        return

    def set_input(self, x):
        """
        Unpack input from dataloader, and preprocess
        """
        a2b = self.direction == GANDirection.A2B

        self.real_A = x["A" if a2b else "B"].to(device=self.device)
        self.real_B = x["B" if a2b else "A"].to(device=self.device)

        self.image_paths = x["A_paths" if a2b else "B_paths"]
        return

    def forward(self):
        self.fake_B = self.net_G_A(self.real_A)
        self.rec_A = self.net_G_B(self.fake_B)

        self.fake_A = self.net_G_B(self.real_B)
        self.rec_B = self.net_G_A(self.fake_A)
        return

    def backward_D_basic(self, net_D, real, fake):
        # Real
        pred_real = net_D(real)
        loss_D_real = self.criterion(pred_real, is_real=True)

        # Fake
        pred_fake = net_D(fake.detach())
        loss_D_fake = self.criterion(pred_fake, is_real=False)

        # Combine
        loss_D = (loss_D_real + loss_D_fake) * 0.5
        loss_D.backward()

        return loss_D

    def backward_D_A(self):
        fake_B = self.fake_B_pool.query(self.fake_B)
        self.loss_D_A = self.backward_D_basic(self.net_D_A, self.real_B, fake_B)
        return

    def backward_D_B(self):
        fake_A = self.fake_A_pool.query(self.fake_A)
        self.loss_D_B = self.backward_D_basic(self.net_D_B, self.real_A, fake_A)
        return

    def backward_G(self):
        lambda_idt = self.lambda_identity
        # Default values from the repo
        lambda_A = 10.0
        lambda_B = 10.0

        if lambda_idt > 0.0:
            # Gen A should be identity if a real B is fed
            self.idt_A = self.net_G_A(self.real_B)
            self.loss_idt_A = self.criterion_idt(self.idt_A, self.real_B) * lambda_B * lambda_idt

            # Gen B should be identity if a real A is fed
            self.idt_B = self.net_G_B(self.real_A)
            self.loss_idt_B = self.criterion_idt(self.idt_B, self.real_A) * lambda_A * lambda_idt
        else:
            self.loss_idt_A = 0.0
            self.loss_idt_B = 0.0

        self.loss_G_A = self.criterion(self.net_D_A(self.fake_B), is_real=True)
        self.loss_G_B = self.criterion(self.net_D_B(self.fake_A), True)

        self.loss_cycle_A = self.criterion_cycle(self.rec_A, self.real_A) * lambda_A
        self.loss_cycle_B = self.criterion_cycle(self.rec_B, self.real_B) * lambda_B

        self.loss = self.loss_G_A + self.loss_G_B + self.loss_cycle_A + self.loss_cycle_B + self.loss_idt_A + self.loss_idt_B
        self.loss.backward()
        return

    def optimize(self):
        self.forward()
        # Discriminators don't need grads when optimizing Generators
        for n in [self.net_D_A, self.net_D_B]:
            for p in n.parameters():
                p.requires_grad = False

        self.optimizer_G.zero_grad()
        self.backward_G()
        self.optimizer_G.step()

        for n in [self.net_D_A, self.net_D_B]:
            for p in n.parameters():
                p.requires_grad = True

        self.optimizer_D.zero_grad()
        self.backward_D_A()
        self.backward_D_B()
        self.optimizer_D.step()

        return

    def update_lr(self):
        """
        Update learning rate - usually at the end of each epoch
        """
        for sched in self.schedulers:
            sched.step()
        return

    def save_models(self, epoch, save_path):
        if os.path.isdir(save_path) == False:
            raise ValueError("{0} is not a directory!".format(save_path))
        for name in self.model_names:
            fn = "{0}/{1}_{2}.pth".format(save_path, name, epoch)
            model = getattr(self, "net_{0}".format(name))
            torch.save(model.state_dict(), fn)
        return

    def load_models(self, epoch, save_path):
        if os.path.isdir(save_path) == False:
            raise ValueError("{0} is not a directory!".format(save_path))
        for name in self.model_names:
            fn = "{0}/{1}_{2}.pth".format(save_path, name, epoch)
            model = getattr(self, "net_{0}".format(name))
            state_dict = torch.load(fn, map_location=self.device)
            model.load_state_dict(state_dict)
        return

    def save_optimizers(self, epoch, save_path):
        if not os.path.isdir(save_path):
            raise ValueError("{0} is not a directory!".format(save_path))
        def _save_optimizer(optimizer, name):
            fn = "{0}/{1}_{2}.pth".format(save_path, name, epoch)
            torch.save(optimizer.state_dict(), fn)
        if self.optimizer_G:
            _save_optimizer(self.optimizer_G, "optimizer_G")
        if self.optimizer_D:
            _save_optimizer(self.optimizer_D, "optimizer_D")
        return

    def load_optimizers(self, epoch, save_path):
        if not os.path.isdir(save_path):
            raise ValueError("{0} is not a directory!".format(save_path))
        def _load_optimizer(optimizer, name):
            fn = "{0}/{1}_{2}.pth".format(save_path, name, epoch)
            state_dict = torch.load(fn)
            optimizer.load_state_dict(state_dict)
        if self.optimizer_G:
            _load_optimizer(self.optimizer_G, "optimizer_G")
        if self.optimizer_D:
            _load_optimizer(self.optimizer_D, "optimizer_D")
        return

    def get_current_loss(self):
        ret = {}
        for name in self.loss_names:
            loss = getattr(self, "loss_{0}".format(name))
            # Detach to prevent unnecessary gradients from being preserved in memory.
            ret[name] = loss.detach()
        return ret

    def print_networks(self):
        nets = [self.net_G_A, self.net_G_B]
        if self.train:
            nets.extend([self.net_D_A, self.net_D_B])
        for net in nets:
            print(net)
        return

    def save_generated_images(self, epoch, save_path):
        if os.path.isdir(save_path) == False:
            raise ValueError("{0} is not a directory!".format(save_path))
        tutils.save_image(self.real_A, "{0}/{1}_{2}.png".format(save_path, "real_A", epoch), normalize=True)
        tutils.save_image(self.real_B, "{0}/{1}_{2}.png".format(save_path, "real_B", epoch), normalize=True)
        tutils.save_image(self.fake_A, "{0}/{1}_{2}.png".format(save_path, "fake_A", epoch), normalize=True)
        tutils.save_image(self.fake_B, "{0}/{1}_{2}.png".format(save_path, "fake_B", epoch), normalize=True)
        tutils.save_image(self.rec_A, "{0}/{1}_{2}.png".format(save_path, "rec_A", epoch), normalize=True)
        tutils.save_image(self.rec_B, "{0}/{1}_{2}.png".format(save_path, "rec_B", epoch), normalize=True)
        return

    def set_multigpu(self, device, ngpu):
        self.net_G_A = nn.DataParallel(self.net_G_A.to(device=device), list(range(ngpu)))
        self.net_G_B = nn.DataParallel(self.net_G_B.to(device=device), list(range(ngpu)))
        if self.train:
            self.net_D_A = nn.DataParallel(self.net_D_A.to(device=device), list(range(ngpu)))
            self.net_D_B = nn.DataParallel(self.net_D_B.to(device=device), list(range(ngpu)))
        return

    def eval(self):
        for name in self.model_names:
            model = getattr(self, "net_{0}".format(name))
            model.eval()
        return

    def test(self):
        with torch.no_grad():
            self.forward()
        return

