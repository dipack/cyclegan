import logging
import torch
import torchvision.transforms as tvtransforms
from argparse import ArgumentParser
from PIL import Image
from cyclegan import CycleGAN
from datasets import GANDirection
from util import clean_make_dir

logging.basicConfig(format='%(asctime)s - %(levelname)s: %(message)s', datefmt='%d/%m/%Y %I:%M:%S %p',
                    level=logging.INFO)

def setup_argparser():
    parser = ArgumentParser()
    parser.add_argument("--a", help="Image A", type=str, required=True)
    parser.add_argument("--b", help="Image B", type=str, required=True)
    parser.add_argument("--weight-path", help="Weights to use", type=str, required=True)
    parser.add_argument("--weight-epoch", help="Epoch weights to use", type=int, required=True)
    parser.add_argument("--outpath", help="Output path", type=str, required=True)
    return parser

def test(image_A, image_B, outpath, weights_path="./output/h2z/weights", test_epoch=10, device=torch.device("cpu"), ngpu=0):
    """
    For generating a single test image
    image_A, and image_B must be torch.Tensor type
    outpath must be where you want to save the generated images
    weights_path must be the path to the model's weights
    test_epoch must be the epoch number of the weight file to use
    """

    clean_make_dir(outpath)

    gan = CycleGAN(train=False, input_num_channels=3, output_num_channels=3, direction=GANDirection.A2B, device=device)
    if device.type == "cuda" and ngpu:
        logging.info("Using multi-GPU cuda with {} devices".format(ngpu))
        gan.set_multigpu(device, ngpu)
    gan.load_models(test_epoch, weights_path)

    # Add Tensor image data here
    test_data = {"A": image_A, "B": image_B, "A_paths": None, "B_paths": None}

    gan.eval()
    gan.set_input(test_data)
    gan.test()
    gan.save_generated_images(1, outpath)

    logging.info("Saved generated images to {}".format(outpath))

    return

def transform_image(img):
    transforms = tvtransforms.Compose([
        tvtransforms.ToTensor(),
        tvtransforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
        ])
    return transforms(img)

if __name__ == "__main__":
    args = setup_argparser().parse_args()

    logging.info("Args: {}".format(args))

    outpath = args.outpath
    weights_path = args.weight_path
    test_epoch = args.weight_epoch

    a = args.a
    b = args.b
    logging.info("Converting images {} and {}".format(a, b))
    image_A = Image.open(a).convert("RGB")
    image_B = Image.open(b).convert("RGB")
    image_A = transform_image(image_A).unsqueeze(0)
    image_B = transform_image(image_B).unsqueeze(0)

    test(image_A, image_B, outpath, weights_path, test_epoch, torch.device("cuda"), 1)

