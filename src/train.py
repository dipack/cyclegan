import collections
import logging
import torch
from argparse import ArgumentParser
from cyclegan import CycleGAN
from datasets import UnalignedDataset, GANDirection, GANPhase
from util import clean_make_dir, plot_losses, plot_scores
from scoring.kid_score import calculate_kid_given_tensors
from scoring.fid import FID

logging.basicConfig(format='%(asctime)s - %(levelname)s: %(message)s', datefmt='%d/%m/%Y %I:%M:%S %p',
                    level=logging.INFO)

def setup_argparser():
    parser = ArgumentParser()
    parser.add_argument("--dataroot", help="Path to dataset", type=str)
    parser.add_argument("--cuda", help="Use CUDA?", action="store_true", default=False)
    parser.add_argument("--ngpu", help="Number of GPUs to use - if multiple", type=int, default=1)
    parser.add_argument("--num-epochs", help="Number of epochs", default=20, type=int)
    parser.add_argument("--log-freq", help="Frequency to log to console", type=int, default=20)
    parser.add_argument("--save-freq", help="Frequency to save to console", type=int, default=20)
    parser.add_argument("--batch-size", help="Batch size", type=int, default=1)
    parser.add_argument("--outroot", help="Path to output", type=str, default="./output")
    parser.add_argument("--fid-freq", help="Frequency to collect images to calculate FID scores", type=int, default=5)
    parser.add_argument("--continue-from", help="Epoch to continue training from", type=int, default=-1)
    return parser

def main():
    args = setup_argparser().parse_args()
    logging.info("Arguments: {}".format(args))

    num_epochs = args.num_epochs
    num_iterations = int(num_epochs * 0.9)
    num_iterations_decay = num_epochs - num_iterations
    batch_size = args.batch_size
    is_cuda = args.cuda
    ngpu = args.ngpu
    dataroot = args.dataroot
    out = args.outroot
    fid_batch_interval = args.fid_freq
    continue_from = args.continue_from

    will_continue = continue_from > -1

    weights = "{0}/weights".format(out)
    images = "{0}/images".format(out)
    if not will_continue:
        clean_make_dir(weights)
        clean_make_dir(images)
    else:
        logging.info("Continuing training from epoch: {}".format(continue_from))

    log_freq = args.log_freq
    save_freq = args.save_freq
    lambda_identity = 0.1

    train_data = UnalignedDataset(dataroot, 3, 3)
    train_data_loader = torch.utils.data.DataLoader(train_data, shuffle=True, batch_size=batch_size)

    test_data = UnalignedDataset(dataroot, 3, 3, phase=GANPhase.Test)
    test_data_loader = torch.utils.data.DataLoader(test_data, shuffle=True, batch_size=batch_size)
    test_dl_iter = iter(test_data_loader)

    if len(train_data_loader) < num_iterations:
        logging.warn("At batch size {0}, max number of iterations is {1} - lowering from user defined iterations of {2}...".format(batch_size, len(train_data_loader), num_iterations))
        num_iterations = len(train_data_loader)

    device = torch.device("cpu")
    if torch.cuda.is_available() and is_cuda:
        logging.info("Using CUDA")
        device = torch.device("cuda")

    gan = CycleGAN(
            train=True,
            num_iterations=num_iterations,
            num_iterations_decay=num_iterations_decay,
            input_num_channels=3,
            output_num_channels=3,
            num_filters=32,
            num_D_layers=2,
            lambda_identity=lambda_identity,
            direction=GANDirection.A2B,
            device=device
            )
    if is_cuda and device.type == "cuda" and ngpu:
        logging.info("Using multi-GPU cuda with {} devices".format(ngpu))
        gan.set_multigpu(device, ngpu)

    if will_continue:
        logging.info("Loading networks from epoch: {} from {}".format(continue_from, weights))
        gan.load_models(continue_from, weights)
        gan.load_optimizers(continue_from, weights)
    gan.print_networks()

    fid_calculator = FID(device=device, transform_input=True)
    fid_scores_A = []
    fid_scores_B = []
    lifetime_losses = collections.defaultdict(list)

    if will_continue:
        logging.info("Skipping {} iterations".format(continue_from - 1))

    for epoch in range(num_epochs):
        if will_continue and epoch + 1 < continue_from:
            continue
        real_A = real_B = None
        fake_A = fake_B = None
        # START - Epoch
        for idx, data in enumerate(train_data_loader):
            gan.set_input(data)
            gan.optimize()
            losses = gan.get_current_loss()

            for lname, lval in losses.items():
                lifetime_losses[lname].append(lval)

            if idx % log_freq == 0:
                epoch_str = "Epoch: [{0}/{1}], Iteration: [{2}/{3}]".format(epoch + 1, num_epochs, idx + 1, len(train_data_loader))
                losses_str = "G_A: {:.4f}, G_B: {:.4f}, D_A: {:.4f}, D_B: {:.4f}, Cycle_A: {:.4f}, Cycle_B: {:.4f}, Idt_A: {:.4f}, Idt_B: {:.4f}".format(losses["G_A"], losses["G_B"], losses["D_A"], losses["D_B"], losses["cycle_A"], losses["cycle_B"], losses["idt_A"], losses["idt_B"])
                logging.info("{0} {1}".format(epoch_str, losses_str))

            if idx % save_freq == 0:
                gan.save_generated_images(epoch + 1, images)
                gan.save_models(epoch + 1, weights)
                gan.save_optimizers(epoch + 1, weights)

            if idx % fid_batch_interval == 0:
                if fake_A is None:
                    fake_A = gan.fake_A.detach()
                else:
                    fake_A = torch.cat((fake_A, gan.fake_A.detach()), dim=0)
                if fake_B is None:
                    fake_B = gan.fake_B.detach()
                else:
                    fake_B = torch.cat((fake_B, gan.fake_B.detach()), dim=0)
        # END - Epoch
        while real_A is None or real_B is None or (len(real_A) < len(fake_A) and len(real_B) < len(fake_B)):
            try:
                data = next(test_dl_iter)

                a2b = gan.direction == GANDirection.A2B

                a = data["A" if a2b else "B"].to(device=device)
                b = data["B" if a2b else "A"].to(device=device)
                if real_A is None:
                    real_A = a
                else:
                    real_A = torch.cat((real_A, a), dim=0)

                if real_B is None:
                    real_B = b
                else:
                    real_B = torch.cat((real_B, b), dim=0)
            except StopIteration:
                test_dl_iter = iter(test_data_loader)
        fid_A = fid_calculator.calculate_fid(real_A, fake_A, batch_size)
        fid_B = fid_calculator.calculate_fid(real_B, fake_B, batch_size)

        fid_scores_A.append(fid_A)
        fid_scores_B.append(fid_B)

        logging.info("FID scores for epoch {} for A: {}".format(epoch + 1, fid_A))
        logging.info("FID scores for epoch {} for B: {}".format(epoch + 1, fid_B))
        gan.update_lr()

    logging.info("FID scores for A: {}".format(fid_scores_A))
    logging.info("FID scores for B: {}".format(fid_scores_B))
    plot_losses(lifetime_losses, "{0}/losses.png".format(out))
    plot_scores(fid_scores_A, "{0}/fid_scores_A.png".format(out))
    plot_scores(fid_scores_B, "{0}/fid_scores_B.png".format(out))
    return

if __name__ == "__main__":
    main()

