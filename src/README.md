# CSE 676, Project 2, CycleGAN
## How to run

### Dependencies
Please see `requirements.txt` to find a list of all required dependencies.

### Commands
You can use the following command to start training the GAN:
```python
python3 train.py --dataroot ./datasets/horse2zebra --outroot ./output/h2z --num-epochs 10 --log-freq 20 --save-freq 20 --batch-size 2 --cuda --ngpu 1
```

To find all supported options, use:
```python
python3 train.py --help
```

Similarly, you can use the following command to generate one-off images using a trained version of the GAN:
```python
python3 test.py --weight-path ./output/h2z/weights --weight-epoch 100 --outpath ./output/test --a ./datasets/horse2zebra/testA/n02381460_1000.jpg --b ./datasets/horse2zebra/testB/n02391049_1000.jpg
```

And supported options can be found using:
```python
python3 test.py --help
```

## References
The authors of the original paper also published their CycleGAN code [here](https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix). We took inspiration from their style and technique.
