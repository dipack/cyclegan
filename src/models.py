import random
import torch
import torch.nn as nn

USE_BIAS = True

class ResnetBlock(nn.Module):
    def __init__(self, channels, padding_type, use_dropout, use_bias):
        super(ResnetBlock, self).__init__()
        block = []
        pad = 0
        if padding_type == "reflect":
            block.extend([nn.ReflectionPad2d(1)])
        elif padding_type == "replicate":
            block.extend([nn.ReplicationPad2d(1)])
        elif padding_type == "zero":
            pad = 1
        else:
            raise NotImplementedError("Padding %s not yet implemented" % padding_type)

        block.extend([
            nn.Conv2d(channels, channels, kernel_size=3, padding=pad, bias=use_bias),
            nn.InstanceNorm2d(channels),
            nn.ReLU(True),
            ])

        if use_dropout:
            block.extend([nn.Dropout(0.5)])

        pad = 0
        if padding_type == "reflect":
            block.extend([nn.ReflectionPad2d(1)])
        elif padding_type == "replicate":
            block.extend([nn.ReplicationPad2d(1)])
        elif padding_type == "zero":
            pad = 1
        else:
            raise NotImplementedError("Padding %s not yet implemented" % padding_type)

        # This time, we don't use a linear rectifier on the output
        block.extend([
            nn.Conv2d(channels, channels, kernel_size=3, padding=pad, bias=use_bias),
            nn.InstanceNorm2d(channels),
            ])

        self.block = nn.Sequential(*block)

        return

    def forward(self, x):
        return x + self.block(x)

class ResnetGenerator(nn.Module):
    """
    Resnet based generator, with Resnet blocks mixed with up,down sampling operations
    """
    def __init__(self, input_num_channels, output_num_channels, num_filters, num_blocks=6, padding_type="reflect", reduce_checkerboard=False):
        super(ResnetGenerator, self).__init__()

        # Input layer
        layers = [
                nn.ReflectionPad2d(3),
                nn.Conv2d(input_num_channels, num_filters, kernel_size=7, padding=0, bias=USE_BIAS),
                nn.InstanceNorm2d(num_filters),
                nn.ReLU(True),
                ]

        # Downsampling layers
        num_downsample = 2
        for idx in range(num_downsample):
            mult = 2 ** idx
            layers.extend([
                nn.Conv2d(num_filters * mult, num_filters * mult * 2, kernel_size=3, stride=2, padding=1, bias=USE_BIAS),
                nn.InstanceNorm2d(num_filters * mult * 2),
                nn.ReLU(True),
                ])

        # Resnet Blocks
        mult = 2 ** num_downsample
        for idx in range(num_blocks):
            layers.extend([
                ResnetBlock(num_filters * mult, padding_type=padding_type, use_dropout=False, use_bias=USE_BIAS)
                ])

        # Upsampling layers
        for idx in range(num_downsample):
            mult = 2 ** (num_downsample - idx)
            if reduce_checkerboard:
                layers.extend([
                    # These three layers instead of a traditional deconv is used to properly 'guess' colours,
                    # and avoid checkerboard like artifacts during image generation.
                    nn.Upsample(scale_factor=2, mode="bilinear"),
                    nn.ReflectionPad2d(1),
                    nn.Conv2d(num_filters * mult, (num_filters * mult) // 2, kernel_size=3, stride=1, padding=0),
                    nn.InstanceNorm2d((num_filters * mult) // 2),
                    nn.ReLU(True),
                    ])
            else:
                layers.extend([
                    nn.ConvTranspose2d(num_filters * mult, (num_filters * mult) // 2, kernel_size=3, stride=2, padding=1, output_padding=1, bias=USE_BIAS),
                    nn.InstanceNorm2d((num_filters * mult) // 2),
                    nn.ReLU(True),
                    ])

        # Output layer
        layers.extend([
            nn.ReflectionPad2d(3),
            nn.Conv2d(num_filters, output_num_channels, kernel_size=7, padding=0),
            nn.Tanh(),
            ])

        self.model = nn.Sequential(*layers)

        return

    def forward(self, x):
        return self.model(x)

class NLayerDiscriminator(nn.Module):
    """
    Simple PatchGAN discriminator
    """
    def __init__(self, input_num_channels, num_filters, num_layers):
        super(NLayerDiscriminator, self).__init__()
        # Add at least one layer
        num_layers = max(1, num_layers)

        # Input layer
        layers = [
                nn.Conv2d(input_num_channels, num_filters, kernel_size=4, stride=2, padding=1),
                nn.LeakyReLU(0.2, True),
                ]

        mult = 1
        mult_prev = 1
        for num in range(1, num_layers):
            mult_prev = mult
            mult = min(2 ** num, 8)
            layers.extend([
                nn.Conv2d(num_filters * mult_prev, num_filters * mult, kernel_size=4, stride=2, padding=1, bias=USE_BIAS),
                nn.InstanceNorm2d(num_filters * mult),
                nn.LeakyReLU(0.2, True),
                ])

        mult_prev = mult
        mult = min(2 ** num_layers, 8)
        layers.extend([
            nn.Conv2d(num_filters * mult_prev, num_filters * mult, kernel_size=4, stride=1, padding=1, bias=USE_BIAS),
            nn.InstanceNorm2d(num_filters * mult),
            nn.LeakyReLU(0.2, True),
            ])

        # Output layer
        layers.extend([
            nn.Conv2d(num_filters * mult, 1, kernel_size=4, stride=1, padding=1),
            ])

        self.model = nn.Sequential(*layers)

        return

    def forward(self, x):
        return self.model(x)

class GANLoss(nn.Module):
    def __init__(self, gan_mode="vanilla", target_real_label=1, target_fake_label=0):
        super(GANLoss, self).__init__()

        self.register_buffer("real_label", torch.tensor(float(target_real_label)))
        self.register_buffer("fake_label", torch.tensor(float(target_fake_label)))

        self.gan_mode = gan_mode

        if gan_mode == "vanilla":
            self.loss = nn.BCEWithLogitsLoss()
        elif gan_mode == "lsgan":
            self.loss = nn.MSELoss()
        elif gan_mode == "wgangp":
            self.loss = None
        else:
            raise NotImplementedError("GAN mode %s not yet implemented" % gan_mode)

        return

    def construct_target_tensor(self, prediction, is_real):
        """
        Create label tensors with same size as input
        """
        if is_real:
            target = self.real_label
        else:
            target = self.fake_label
        return target.expand_as(prediction)

    def __call__(self, prediction, is_real):
        """
        Calculate loss, given prediction and ground truth
        """
        if self.gan_mode in ["lsgan", "vanilla"]:
            target = self.construct_target_tensor(prediction, is_real)
            loss = self.loss(prediction, target)
        elif self.gan_mode == "wgangp":
            if is_real:
                loss = -prediction.mean()
            else:
                loss = prediction.mean()
        return loss

class ImagePool():
    """This class implements an image buffer that stores previously generated images.
    This buffer enables us to update discriminators using a history of generated images
    rather than the ones produced by the latest generators.
    """

    def __init__(self, pool_size):
        """Initialize the ImagePool class
        Parameters:
            pool_size (int) -- the size of image buffer, if pool_size=0, no buffer will be created
        """
        self.pool_size = pool_size
        if self.pool_size > 0:  # create an empty pool
            self.num_imgs = 0
            self.images = []

    def query(self, images):
        """Return an image from the pool.
        Parameters:
            images: the latest generated images from the generator
        Returns images from the buffer.
        By 50/100, the buffer will return input images.
        By 50/100, the buffer will return images previously stored in the buffer,
        and insert the current images to the buffer.
        """
        if self.pool_size == 0:  # if the buffer size is 0, do nothing
            return images
        return_images = []
        for image in images:
            image = torch.unsqueeze(image.data, 0)
            if self.num_imgs < self.pool_size:   # if the buffer is not full; keep inserting current images to the buffer
                self.num_imgs = self.num_imgs + 1
                self.images.append(image)
                return_images.append(image)
            else:
                p = random.uniform(0, 1)
                if p > 0.5:  # by 50% chance, the buffer will return a previously stored image, and insert the current image into the buffer
                    random_id = random.randint(0, self.pool_size - 1)  # randint is inclusive
                    tmp = self.images[random_id].clone()
                    self.images[random_id] = image
                    return_images.append(tmp)
                else:       # by another 50% chance, the buffer will return the current image
                    return_images.append(image)
        return_images = torch.cat(return_images, 0)   # collect all the images and return
        return return_images
