import os
import random
import numpy as np
import torch.utils.data as tdata
import torchvision.transforms as tvtransforms
from PIL import Image

IMAGE_EXTS = [".png", ".jpg", ".jpeg"]

class GANPhase:
    Train = "train"
    Test = "test"

class GANDirection:
    A2B = "A2B"
    B2A = "B2A"

def make_dataset(directory, max_len=float("inf")):
    images = []
    assert os.path.isdir(directory), "{0} is not a directory".format(directory)

    for root, _, fnames in sorted(os.walk(directory)):
        for fn in fnames:
            if any(fn.lower().endswith(ext) for ext in IMAGE_EXTS):
                path = os.path.join(root, fn)
                images.append(path)
    return np.asarray(images[:min(max_len, len(images))])

class UnalignedDataset(tdata.Dataset):
    def __init__(self, root, input_num_channels, output_num_channels, phase=GANPhase.Train, direction=GANDirection.A2B, serial=True, transforms=None):
        """
        serial - fetch B image with same index as A
        """
        self.root = root
        self.serial = serial
        self.dir_A = os.path.join(self.root, "{0}A".format(phase))
        self.dir_B = os.path.join(self.root, "{0}B".format(phase))

        self.paths_A = sorted(make_dataset(self.dir_A))
        self.paths_B = sorted(make_dataset(self.dir_B))

        self.size_A = len(self.paths_A)
        self.size_B = len(self.paths_B)

        a2b = direction == GANDirection.A2B

        input_nc = input_num_channels if a2b else output_num_channels
        output_nc = output_num_channels if a2b else input_num_channels

        if transforms is None:
            transforms = [
                tvtransforms.ToTensor(),
                ]

        self.tranform_A = transforms
        self.tranform_B = transforms

        # If there's only channel, it is grayscale
        if input_nc == 1:
            self.tranform_A.append(tvtransforms.Normalize((0.5, ), (0.5, )))
        # else RGB
        else:
            self.tranform_A.append(tvtransforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)))

        if output_nc == 1:
            self.tranform_B.append(tvtransforms.Normalize((0.5, ), (0.5, )))
        else:
            self.tranform_B.append(tvtransforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)))

        self.tranform_A = tvtransforms.Compose(self.tranform_A)
        self.tranform_B = tvtransforms.Compose(self.tranform_B)

        return

    def __getitem__(self, index):
        pA = self.paths_A[index % self.size_A]
        pB = self.paths_B[index % self.size_B] if self.serial else self.paths_B[random.randint(0, self.size_B - 1)]

        img_A = Image.open(pA).convert("RGB")
        img_B = Image.open(pB).convert("RGB")

        return {"A": self.tranform_A(img_A), "B": self.tranform_B(img_B), "A_paths": pA, "B_paths": pB}

    def __len__(self):
        """
        Since datasets are unaligned, we could have unequal lengths
        """
        return max(self.size_A, self.size_B)
