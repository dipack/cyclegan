# Unpaired Image-to-Image translation with CycleGAN

## Problem Statement
Image-to-image translation is a class of vision and graphics problems where the goal is to learn the mapping between an input image and an output image using a training set of aligned image pairs.

However, for many tasks, paired training data will not be available. We present an approach for learning to translate an image from a source domain X to a target domain Y in the absence of paired examples.

## Technique
CycleGAN is an approach to training image-to-image translation models using the generative adversarial network, or GAN, model architecture.

The CycleGAN is an extension of the GAN architecture that involves the simultaneous training of two generator models and two discriminator models.

One generator takes images from the first domain as input and outputs images for the second domain, and the other generator takes images from the second domain as input and generates images for the first domain. Discriminator models are then used to determine how plausible the generated images are and update the generator models accordingly..

The CycleGAN uses an additional extension to the architecture called cycle consistency. This is the idea that an image output by the first generator could be used as input to the second generator and the output of the second generator should match the original image. The reverse is also true: that an output from the second generator can be fed as input to the first generator and the result should match the input to the second generator.

Cycle consistency is a concept from machine translation where a phrase translated from English to French should translate from French back to English and be identical to the original phrase. The reverse process should also be true.

## Datasets
1. Apple to Orange
2. Yosemite - Summer to Winter
3. Monet to Photo
4. Horse to Zebra

## Evaluation Metrics
Kernel Inception Distance (KID), and eyeball evaluation

## Team Members
Dipack P Panjabi (dipackpr@buffalo.edu), Prashant Singh (psingh37@buffalo.edu)
